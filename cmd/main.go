package main


import (
    "os"
    "log"
    "runtime"
    "net/http"
    "os/signal"
    "github.com/ark1790/distributor/ui"
    "github.com/ark1790/distributor/config"
    "github.com/ark1790/distributor/worker"
)

func main() {
    runtime.GOMAXPROCS(runtime.NumCPU())

    err := conf.LoadFile("distributor.conf")
    if err != nil {
        log.Fatal(err)
    }
    //
    // worker.Init()
    go worker.Run()


    http.Handle("/", ui.Router)

    go func() {
        err := http.ListenAndServe(":8997" , nil)
        if err != nil {
            log.Fatal(err)
        }
    }()


    c := make(chan os.Signal, 1)
    signal.Notify(c , os.Interrupt)

    log.Printf("Received %s; exiting.." , <-c)
    log.Println("Stopping Worker")
    worker.Stop()
    log.Println("Stopped Worker")

    log.Println("Deleting all workers")
    for ; worker.WorkerCount > 0 ;{
        worker.StopWorker()
    }

}
