package worker

import (
    "os"
    "log"
    // // "os/exec"
    // // "runtime"
    // // "strconv"
    // // "io/ioutil"
    // // "encoding/json"
    "path/filepath"
    "github.com/ark1790/distributor/config"
    "github.com/ark1790/distributor/nginx"
)

type Balancer struct{
    Name        string
    Port        int
    LinodeID    string
    DiskID      string
    HostName    string
}

var Server = struct {
    Apps    []struct{
        Name            string `json:"name"`
        Script          string `json:"script"`
        Log_date_format string `json:"log_date_format"`
        Env  struct {
            NODE_ENV    string `json:"NODE_ENV"`
            Port        string `json:"PORT"`
        } `json:"env"`
    } `json:"apps"`
}{}




func UpdateNginxConfig(count uint) {

    newList := make([]Balancer , count)
    copy(newList , BalancerList[0:count])

    f , err := os.Create(filepath.Join(conf.Curr.Nginx.Dir , "default"))

    err = nginx.TplConfigNginx.Execute( f , struct{
        Bal []Balancer
    }{
        Bal: newList,
    })

    if err != nil {
        log.Print()
    }
}

var BalancerList [] Balancer

// func init(){
//     BalancerList = make([] Balancer , 0)
// }
