package worker


import (
    // "os"
    // "sync"
    "time"
    "log"
    // "os/exec"
    // "path/filepath"
    "github.com/ark1790/distributor/linode"
    "github.com/ark1790/distributor/config"
)

var WorkerCount uint = 0
var DeleteInProgress bool = false
var CreateInProgress bool = false

func trace(err error){
    if err != nil {
        log.Println(err)
        panic(err)
    }
}

func DeployWorker() {
    c := linode.NewClient(conf.Curr.Linode.ApiKey)

    r := c.NewRequest()
    linodeID , err := r.Create(conf.Curr.Linode.Datacenterid , conf.Curr.Linode.Planid)
    trace(err)

    r = c.NewRequest()
    diskID , err := r.DeployFromImage( conf.Curr.Linode.Imageid , linodeID)
    trace(err)

    r = c.NewRequest()
    err = r.CreateConfig( diskID , linodeID)
    trace(err)

    r = c.NewRequest()
    err = r.Boot(linodeID)
    trace(err)

    r = c.NewRequest()
    ipaddress , err := r.GetIP(linodeID)
    trace(err)

    log.Println("IP" , ipaddress)

    bal := Balancer{ Name : "Celica" , Port : 8000 , LinodeID : linodeID , HostName : ipaddress , DiskID : diskID }
    BalancerList = append(BalancerList ,  bal)

    log.Println("Balancer List" , BalancerList)

    WorkerCount += 1
    CreateInProgress = false
}

func StopWorker() {
    if WorkerCount < 1 {
        return
    }

    bal := BalancerList[WorkerCount-1]

    c := linode.NewClient(conf.Curr.Linode.ApiKey)

    r := c.NewRequest()
    err := r.Shutdown(bal.LinodeID)
    trace(err)

    r = c.NewRequest()
    err = r.DeleteDisk( bal.DiskID , bal.LinodeID )
    trace(err)

    log.Println("DELETING LINODE" , bal.LinodeID)
    r = c.NewRequest()
    fl , err := r.IsAnyJobPending( bal.LinodeID )
    trace(err)

    log.Println("INSIDE INSIDE INSIDE")
    for ; !fl  ; {
        r = c.NewRequest()
        fl , err = r.IsAnyJobPending( bal.LinodeID )
        trace(err)
        log.Println("NOT DELETED LINODE" , bal.LinodeID)
        time.Sleep(1 * time.Second)
    }



    r = c.NewRequest()
    err = r.Delete( bal.LinodeID )
    trace(err)

    log.Println("DELETED LINODE" , bal.LinodeID)

    WorkerCount -= 1

    DeleteInProgress = false

}
