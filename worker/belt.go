package worker


import (
    "math"
    "log"
    "time"
    "strings"
    "strconv"
    // "runtime"
    "net/http"
    "io/ioutil"
    // "github.com/shirou/gopsutil/mem"
    "github.com/ark1790/distributor/config"
    "github.com/ark1790/distributor/nginx"
)


var queue []uint



var stop = false

var fl bool = false

func avg(x uint, t uint) (uint , uint) {
    queue[t%900] = x

    var rms uint = 0
    var average uint = 0

    for i := 0 ; i < 900 ; i++ {
        average += queue[i]
        rms += (queue[i]*queue[i])
    }

    average /= 900
    rms = (rms / 900)
    rms = uint((math.Sqrt(float64(rms))))

    return rms , average
}

func Run(){
    var t uint = 0
    log.Println("Initializing Belt...")

    queue = make([]uint , 905)

    for i := 0 ; i < 900 ; i++ {
        queue[i] = 50
    }

    for ; !stop ;  {
        curCount :=  hit()
        rms , average := avg(curCount , t )
        mxCon := conf.Curr.Limit.Threshold

        log.Println("Average hits:" , average)
        log.Println("RMS hits:" , rms)

        var reqWorkerStarted uint = 0
        var reqWorkerStopped uint = 0
        reqWorkerStarted = (rms/mxCon)
        reqWorkerStopped = (rms/mxCon)


        if rms % mxCon != 0 {
            reqWorkerStarted += 1
        }

        if rms % mxCon != 0 {
            reqWorkerStopped += 1
        }

        if reqWorkerStarted <= 1 {
            reqWorkerStarted = 2
        }

        if reqWorkerStopped <= 1 {
            reqWorkerStopped = 2
        }

        if reqWorkerStarted > WorkerCount && WorkerCount < conf.Curr.Worker.Max && !CreateInProgress{
            log.Println("Deployed new worker")
            CreateInProgress = true
            go func(){
                DeployWorker()
                UpdateNginxConfig(WorkerCount)
                nginx.Reload()
            }()
        }


        if reqWorkerStopped < WorkerCount && WorkerCount > conf.Curr.Worker.Min && !DeleteInProgress {
            log.Println("Stopped existing worker")
            DeleteInProgress = true
            go func(){
                StopWorker()
                UpdateNginxConfig(WorkerCount)
                nginx.Reload()
            }()

        }

        log.Println("Worker Count:" , WorkerCount)
        t = (t + 1) % 1000

        time.Sleep(1 * time.Second)
    }

}

func hit() uint{
    resp , err := http.Get("http://localhost:8888/status")
    if err != nil {
        panic(err)
    }

    defer resp.Body.Close()

    body , err := ioutil.ReadAll(resp.Body)

    if err != nil {
        log.Println(err)
        panic(err)
    }

    Body := string(body[:])
    Lines := strings.Split(Body , `\n`)
    Lines1 := strings.Split(Lines[0] , ` `)

    conn , err := strconv.Atoi(Lines1[2])

    if err != nil {
        log.Println(err)
        panic(err)
    }

    return uint(conn)
}

func Stop(){
    stop = true
    time.Sleep(2 * time.Second)
}
