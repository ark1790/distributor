package linode

import (
    // "fmt"
    "log"
    "net/http"
    "strconv"
    "encoding/json"
)

var LinodeIDs []int

func (r *Request)Create(Did string , Pid string) (string , error) {
    d := make(map[string]string)

    d["DatacenterID"] = Did
    d["PlanID"] = Pid

    r = r.AddAction("linode.create" , d)

    u , _ := r.URL()
    resp, err := http.Get(u)
    if err != nil {
        return "" , err
    }
    defer resp.Body.Close()

    var responseJSONs[] response
    decoder := json.NewDecoder(resp.Body)
    decoder.Decode(&responseJSONs)

    linodes := struct {
        LinodeID int `json:"LinodeID"`
    }{}
    if err = json.Unmarshal(responseJSONs[0].Data, &linodes); err != nil {
		return "" , err
	}

    return strconv.Itoa(linodes.LinodeID) , nil
}

func (r *Request)Delete(ID string) error {
    d := make(map[string]string)
    d["LinodeID"] = ID


    r = r.AddAction("linode.delete" , d)

    u , _ := r.URL()
    log.Println("URL" , u)
    resp, err := http.Get(u)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    var responseJSONs[] response
    decoder := json.NewDecoder(resp.Body)
    decoder.Decode(&responseJSONs)

    linodes := struct {
        LinodeID int `json:"LinodeID"`
    }{}

    if err = json.Unmarshal(responseJSONs[0].Data, &linodes); err != nil {
		return err
	}

    return nil
}
