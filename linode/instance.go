package linode

import (
    "log"
    "encoding/json"
    "net/http"
)

func (r *Request) Boot(LinodeID string) ( error) {
    d := make(map[string]string)

    d["LinodeID"] = LinodeID

    r = r.AddAction("linode.boot" , d)

    u , _ := r.URL()
    resp, err := http.Get(u)
    if err != nil {
        log.Println(err)
        return err
    }
    defer resp.Body.Close()

    var responseJSONs[] response
    decoder := json.NewDecoder(resp.Body)
    decoder.Decode(&responseJSONs)

    return nil
}

func (r *Request) Reboot(LinodeID string) ( error) {
    d := make(map[string]string)

    d["LinodeID"] = LinodeID

    r = r.AddAction("linode.reboot" , d)

    u , _ := r.URL()
    resp, err := http.Get(u)
    if err != nil {
        log.Println(err)
        return err
    }
    defer resp.Body.Close()

    var responseJSONs[] response
    decoder := json.NewDecoder(resp.Body)
    decoder.Decode(&responseJSONs)

    return nil
}

func (r *Request) Shutdown(LinodeID string) ( error) {
    d := make(map[string]string)

    d["LinodeID"] = LinodeID

    r = r.AddAction("linode.shutdown" , d)

    u , _ := r.URL()
    resp, err := http.Get(u)
    if err != nil {
        log.Println(err)
        return err
    }
    defer resp.Body.Close()

    var responseJSONs[] response
    decoder := json.NewDecoder(resp.Body)
    decoder.Decode(&responseJSONs)

    return nil
}

func (r *Request) GetIP(LinodeID string) ( string , error) {
    d := make(map[string]string)

    d["LinodeID"] = LinodeID

    r = r.AddAction("linode.ip.list" , d)

    u , _ := r.URL()
    resp, err := http.Get(u)
    if err != nil {
        log.Println(err)
        return "" , err
    }
    defer resp.Body.Close()

    var responseJSONs[] response
    decoder := json.NewDecoder(resp.Body)
    decoder.Decode(&responseJSONs)

    body := []struct{
        LINODEID uint
        ISPUBLIC uint
        IPADDRESS string
        RDNS_NAME string
        IPADDRESSID uint
    }{}

    if err = json.Unmarshal(responseJSONs[0].Data, &body); err != nil {
        log.Println(err)
    	return "" , err
	}

    return body[0].IPADDRESS , nil
}
