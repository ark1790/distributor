package linode

import (
    // "fmt"
    "log"
    "net/http"
    "strconv"
    "encoding/json"
)


func (r *Request) DeployFromImage(ImageID string , LinodeID string) ( string , error) {
    d := make(map[string]string)

    d["ImageID"] = ImageID
    d["LinodeID"] = LinodeID
    d["rootPass"] = "Rootify"



    r = r.AddAction("linode.disk.createfromimage" , d)

    u , _ := r.URL()
    resp, err := http.Get(u)
    if err != nil {
        return "" , err
    }
    defer resp.Body.Close()

    var responseJSONs[] response
    decoder := json.NewDecoder(resp.Body)
    decoder.Decode(&responseJSONs)


    body := struct{
        DISKID  int
        JOBID   int
    }{}

    if err = json.Unmarshal(responseJSONs[0].Data, &body); err != nil {
        log.Println(err)
    	return "" , err
	}

    log.Println("DISK CREATED FROM IMAGE" , body.DISKID)

    return strconv.Itoa(body.DISKID) , nil
}


func (r *Request) CreateConfig(DiskID string , LinodeID string) ( error) {
    d := make(map[string]string)

    d["DiskList"] = DiskID
    d["LinodeID"] = LinodeID
    d["KernelID"] = "229"
    d["Label"] = "Celica_configuration"

    r = r.AddAction("linode.config.create" , d)

    u , _ := r.URL()
    resp, err := http.Get(u)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    var responseJSONs[] response
    decoder := json.NewDecoder(resp.Body)
    decoder.Decode(&responseJSONs)

    return nil
}

func (r *Request) DeleteDisk(DiskID string , LinodeID string) ( error) {
    d := make(map[string]string)

    d["DiskID"] = DiskID
    d["LinodeID"] = LinodeID


    r = r.AddAction("linode.disk.delete" , d)

    u , _ := r.URL()
    resp, err := http.Get(u)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    var responseJSONs[] response
    decoder := json.NewDecoder(resp.Body)
    decoder.Decode(&responseJSONs)

    return nil
}
