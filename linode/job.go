package linode

import (
    "log"
    "encoding/json"
    "net/http"
)

func (r *Request) IsAnyJobPending( LinodeID string) ( bool , error) {
    d := make(map[string]string)

    d["LinodeID"] = LinodeID
    d["pendingOnly"] = "1"

    r = r.AddAction("linode.job.list" , d)

    u , _ := r.URL()
    log.Println(u)
    resp, err := http.Get(u)
    if err != nil {
        log.Println(err)
        return false , err
    }
    defer resp.Body.Close()

    var responseJSONs[] response
    decoder := json.NewDecoder(resp.Body)
    decoder.Decode(&responseJSONs)

    body := []interface{}{}

    if err = json.Unmarshal(responseJSONs[0].Data, &body); err != nil {
		return false , err
	}




    return len(body) == 0 ,  nil
}
