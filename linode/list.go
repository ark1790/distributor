package linode

import (
    // "fmt"
    "encoding/json"
    "net/http"
)

type response struct {
	Action string `json:"ACTION"`
	Errors []struct {
		Code    int    `json:"ERRORCODE"`
		Message string `json:"ERRORMESSAGE"`
	} `json:"ERRORARRAY,omitempty"`
	Data json.RawMessage `json:"DATA,omitempty"`
}


type Linode struct {
	ID           int    `json:"LINODEID"`
	Status       int    `json:"STATUS"`
	Label        string `json:"LABEL"`
	DisplayGroup string `json:"LPM_DISPLAYGROUP"`
	RAM          int    `json:"TOTALRAM"`
}

func (r *Request) GetList() ([]Linode , error){
    r.AddAction("linode.list" , nil)

    u , _ := r.URL()
    resp, err := http.Get(u)
    if err != nil {
        return nil , err
    }

    defer resp.Body.Close()

    var responseJSONs[] response
    decoder := json.NewDecoder(resp.Body)
    decoder.Decode(&responseJSONs)

    var linodes []Linode
    if err = json.Unmarshal(responseJSONs[0].Data, &linodes); err != nil {
		return nil , err
	}

    // fmt.Println(linodes)

    return linodes , nil
}
