
package linode

import (
    // "log"
    "encoding/json"
    "net/url"
)

const apiEndpoint = "https://api.linode.com/"

var apiEndpointURL *url.URL

func init() {
    apiEndpointURL , _ = url.Parse(apiEndpoint)
}

type Client struct {
    apiKey string
}

func NewClient(apiKey string) *Client {
    return &Client{apiKey : apiKey}
}

type action map[string]string

type Request struct {
    client Client
    actions []action
}

func (c *Client) NewRequest() *Request {
    return &Request{ client : *c }
}

func (r *Request) AddAction(method string , params map[string]string) *Request {
    var a action
    if params == nil {
        a = make(action)
    } else {
        a = action(params)
    }
    a["api_action"] = method
    r.actions = append(r.actions , a)

    return r
}


func (r *Request) URL() (string , error){
    numActions := len(r.actions)
    if numActions == 0 {
        return "" , nil
    }

    actionBatch := make([]action , 0)

    for _ , act := range r.actions {
        actionBatch = append(actionBatch , act)
    }

    // var urls string

    params := make(url.Values)
    params.Set("api_key" , r.client.apiKey)
    params.Set("api_action" , "batch")
    requestArrayValue , err := json.Marshal(actionBatch)

    if err != nil {
        return "" , err
    }
    // log.Println(string(requestArrayValue))
    params.Set("api_requestArray" , string(requestArrayValue))
    // log.Println(params)
    u := apiEndpointURL
    u.RawQuery = params.Encode()

    return u.String() , nil
}
