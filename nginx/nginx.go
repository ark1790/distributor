package nginx

import (
    "sync"
    "os/exec"
    "log"
)

type Nginx struct {
    sync.Mutex
}

func (n *Nginx) Start() {
    n.Lock()
    defer n.Unlock()

    cmd := exec.Command("sudo" , "service"  , "nginx", "start")

    err := cmd.Run()

    if err != nil {
        log.Print(err)
    }
}



func (n *Nginx) Stop(){
    n.Lock()
    defer n.Unlock()

    cmd := exec.Command("sudo" , "service"  , "nginx" , "stop")

    err := cmd.Run()

    if err != nil {
        log.Print(err)
    }
}

func (n *Nginx) Reload() {
    n.Lock()
    defer n.Unlock()

    cmd := exec.Command("sudo" , "service" , "nginx", "reload")

    err := cmd.Run()

    if err != nil {
        log.Print(err)
    }
}

var DefaultNginx Nginx

func Start(){
    DefaultNginx.Start()
}

func Stop(){
    DefaultNginx.Stop()
}

func Reload(){
    DefaultNginx.Reload()
}
