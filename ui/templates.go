package ui

import (
    "html/template"
)

var (
    TplLayout = template.Must(template.New("layout.html").ParseFiles("ui/templates/layout.html"))

    // Serve Index Page

    TplIndex = template.Must( template.Must(TplLayout.Clone()).ParseFiles("ui/templates/index.html") )
    TplList = template.Must( template.Must(TplLayout.Clone()).ParseFiles("ui/templates/list.html"))

    TplCreateNewLinode = template.Must( template.Must(TplLayout.Clone()).ParseFiles("ui/templates/create.html"))
)
