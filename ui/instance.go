package ui

import (
    "log"
    "net/http"
    "github.com/gorilla/mux"
    "github.com/ark1790/distributor/linode"
    // "github.com/gorilla/schema"
    // "github.com/ark1790/distributor/linode"
    "github.com/ark1790/distributor/config"
)


func ServeEditLinode(w http.ResponseWriter , r *http.Request){
    vars := mux.Vars(r)
    log.Println(vars)
    http.Redirect(w , r , "/linode/list", http.StatusSeeOther)
}

func HandleLinodeBoot(w http.ResponseWriter , r *http.Request){
    vars := mux.Vars(r)
    log.Println("Booting Linode:" , vars["id"])

    c := linode.NewClient(conf.Curr.Linode.ApiKey)
    req := c.NewRequest()

    err := req.Boot(vars["id"])
    if err != nil {
        log.Println(err)
    }

    http.Redirect(w , r , "/linode/list", http.StatusSeeOther)
}

func HandleLinodeReboot(w http.ResponseWriter , r *http.Request){
    vars := mux.Vars(r)
    log.Println("Rebooting Linode:" , vars["id"])

    c := linode.NewClient(conf.Curr.Linode.ApiKey)
    req := c.NewRequest()

    err := req.Reboot(vars["id"])
    if err != nil {
        log.Println(err)
    }

    http.Redirect(w , r , "/linode/list", http.StatusSeeOther)
}

func HandleLinodeShutdown(w http.ResponseWriter , r *http.Request){
    vars := mux.Vars(r)
    log.Println("Shutting Down Linode:" , vars["id"])

    c := linode.NewClient(conf.Curr.Linode.ApiKey)
    req := c.NewRequest()

    err := req.Shutdown(vars["id"])
    if err != nil {
        log.Println(err)
    }

    http.Redirect(w , r , "/linode/list", http.StatusSeeOther)
}

func init(){

    Router.NewRoute().
        Methods("POST").
        Path("/linode/edit/{id}").
        Handler(http.HandlerFunc(ServeEditLinode))

    Router.NewRoute().
        Methods("GET").
        Path("/linode/{id}/boot").
        Handler(http.HandlerFunc(HandleLinodeBoot))

    Router.NewRoute().
        Methods("GET").
        Path("/linode/{id}/reboot").
        Handler(http.HandlerFunc(HandleLinodeReboot))

    Router.NewRoute().
        Methods("GET").
        Path("/linode/{id}/shutdown").
        Handler(http.HandlerFunc(HandleLinodeShutdown))
}
