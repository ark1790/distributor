

package ui

import (
    "log"
    "net/http"
    // "github.com/gorilla/mux"
    "github.com/gorilla/schema"
    "github.com/ark1790/distributor/linode"
    "github.com/ark1790/distributor/config"
)


func ServeCreateNewLinode(w http.ResponseWriter , r *http.Request) {
    err := TplCreateNewLinode.Execute(w , "")
    if err != nil {
        panic(err)
    }
}


func HandleCreateNewLinode(w http.ResponseWriter , r *http.Request) {
    err := r.ParseForm()
    if err != nil {
        http.Error(w , "Bad Request" , http.StatusBadRequest)
        return
    }

    body := struct {
        DataCenterID string `schema: "datacenterId"`
        PlanID       string `schema: "planId"`
    }{}

    err = schema.NewDecoder().Decode(&body, r.PostForm)
	if err != nil {
        log.Println(err)
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

    c := linode.NewClient(conf.Curr.Linode.ApiKey)
    req := c.NewRequest()

    req.Create(body.DataCenterID , body.PlanID)

    http.Redirect(w , r , "/create", http.StatusSeeOther)
}



func ServeLinodeList(w http.ResponseWriter , r *http.Request){
    c := linode.NewClient(conf.Curr.Linode.ApiKey)
    req := c.NewRequest()
    l , err := req.GetList()

    if err != nil {
        log.Println(err)
        return
    }

    err = TplList.Execute(w , struct{
        Linodes []linode.Linode
    }{
        Linodes : l,
    })

    if err != nil {
        log.Println(err)
        return
    }
}




func init(){
    Router.NewRoute().
        Methods("GET").
        Path("/linode/list").
        Handler(http.HandlerFunc(ServeLinodeList))

    Router.NewRoute().
        Methods("GET").
        Path("/linode/create").
        Handler(http.HandlerFunc(ServeCreateNewLinode))

    Router.NewRoute().
        Methods("POST").
        Path("/linode/create").
        Handler(http.HandlerFunc(HandleCreateNewLinode))


}
