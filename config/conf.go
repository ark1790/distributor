package conf

import(
    "os"
    "github.com/naoina/toml"
)

var Curr = struct {
    System struct {
        Host            string
        Directory       string
    }
    Limit struct{
        Threshold       uint
    }
    Nginx struct {
        Dir             string
    }
    Linode struct {
        ApiKey          string
        Datacenterid    string
        Planid          string
        Imageid         string
    }
    Worker struct {
        Min             uint
        Max             uint
    }
}{}

func LoadFile(name string) error{
    ff , err := os.Open(name)

    if os.IsNotExist(err) {
        ff , err = os.Create(name)
        if err != nil {
            return err
        }

        err = toml.NewEncoder(ff).Encode(Curr)
        if err != nil {
            return err
        }

        err = ff.Close()
        if err != nil{
            return err
        }

        return nil
    }

    if err != nil {
        return err
    }

    err = toml.NewDecoder(ff).Decode(&Curr)
    if err != nil {
        return err
    }

    return nil
}

func init() {
    Curr.System.Host = ":8998"

    Curr.Limit.Threshold = 50

    Curr.Nginx.Dir = "./"

    Curr.Linode.ApiKey = "ciQExFoQsaIoEP2J43tvpfrLJqUMvFYgSXKfGnKJp4b6Dtx6CfgpMjJicNUhjhH6"
    Curr.Linode.Datacenterid = "2"
    Curr.Linode.Planid = "2"
    Curr.Linode.Imageid = "736397"

    Curr.Worker.Min = 2
    Curr.Worker.Max = 4
}
